
.PHONY: default all test clean distclean

default: all

$(VERBOSE).SILENT:

package = game
version = 0.1.0

LOVE = love

ZIP = zip -u
ZIPFLAGS = -9 -q

RM = rm -f
RM_R = rm -fr
CP = cp -f
CP_R = cp -fr
MKDIR_P = mkdir -p

kutils_lua_files = \
	lib/kutils/class/init.lua \
	lib/kutils/class/Class.lua \
	lib/kutils/class/Object.lua \
	lib/kutils/exceptions/init.lua \
	lib/kutils/exceptions/Exception.lua \
	lib/kutils/config.lua \
	lib/kutils/expect.lua \
	lib/kutils/glob.lua \
	lib/kutils/minser.lua \
	lib/kutils/stringex.lua \
	lib/kutils/tableex.lua \
	lib/kutils/types.lua

game_lua_files = \
	lib/game/entity/init.lua \
	lib/game/entity/Entity.lua \
	lib/game/map/init.lua \
	lib/game/map/entreg.lua \
	lib/game/map/Map.lua \
	lib/game/map/TileSet.lua \
	lib/game/map/Layer.lua \
	lib/game/map/TileLayer.lua \
	lib/game/map/EntityLayer.lua \
	lib/game/map/ImageLayer.lua \
	lib/game/init.lua \
	lib/game/main.lua \
	lib/game/intl.lua \
	lib/game/res.lua \
	lib/game/state.lua \
	lib/game/Box.lua \
	conf.lua \
	main.lua

game_data_files = \
	data/gfx/player.png \
	data/gfx/tileset.png \
	data/maps/test.lua

all_files = \
	$(kutils_lua_files) \
	$(game_data_files) \
	$(game_lua_files)

dist_dir = $(package)-$(version)

dist_love = $(dist_dir)/$(package).love

dist_files = \
	$(dist_dir)/README.md \
	$(dist_dir)/LICENSE.md \
	$(dist_dir)/run.sh \
	$(dist_love)

dist_zip = $(package)-$(version).zip

all: $(dist_zip)

$(dist_zip): $(dist_dir)/.timestamp $(dist_files)
	@echo ZIP $(dist_zip)
	$(ZIP) $(ZIPFLAGS) $(dist_zip) $(dist_files)

$(dist_love): $(dist_dir)/.timestamp $(all_files)
	@echo ZIP $(package).love
	$(ZIP) $(ZIPFLAGS) $(dist_love) $(all_files)

$(dist_dir)/LICENSE.md: $(dist_dir)/.timestamp LICENSE.md
	@echo CP LICENSE.md
	$(CP) LICENSE.md $@

$(dist_dir)/README.md: $(dist_dir)/.timestamp README.md
	@echo CP README.md
	$(CP) README.md $@

$(dist_dir)/run.sh: $(dist_dir)/.timestamp misc/run.sh
	@echo CP run.sh
	$(CP) misc/run.sh $@

$(dist_dir)/.timestamp:
	@$(MKDIR_P) $(dist_dir)
	@date "+%Y-%m-%d %H:%M:%S" > $(dist_dir)/.timestamp

test: $(dist_love)
	$(LOVE) $(dist_love)

clean:
	@echo RM $(dist_dir)
	$(RM_R) $(dist_dir)

distclean: clean
	@echo RM $(dist_zip)
	$(RM) $(dist_zip)
