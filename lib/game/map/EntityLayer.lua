
---
-- Entity layer.
--
-- **Extends:** `game.map.Layer`
--
-- @classmod game.map.EntityLayer

local class = require "class"

local entreg = require "game.map.entreg"
local deepcopy = require "tableex".deepcopy

local Layer = require "game.map.Layer"

local EntityLayer = class("game.map.EntityLayer", Layer)

local function loadobject(obj)
	local t = obj.type
	assert(t and t~="", "object has no type")
	assert(obj.shape == "rectangle", "unsupported object shape")
	local params = deepcopy(obj.properties)
	params.x = obj.x
	params.y = obj.y
	return entreg.new(t, params)
end

---
-- Construct a new layer.
--
-- @tparam tiled.Layer data
function EntityLayer:init(data)
	assert(data.type == "objectgroup", "invalid data specified")
	Layer.init(self, data)
	local ents = { }
	for i, obj in ipairs(data.objects) do
		ents[i] = loadobject(obj)
	end
	self.entities = ents
end

---
-- Draw the layer.
--
-- **Overrides:** `game.map.Layer:draw`
function EntityLayer:draw()
end

---
-- Update the layer logic.
--
-- **Overrides:** `game.map.Layer:update`
function EntityLayer:update()
end

return EntityLayer
