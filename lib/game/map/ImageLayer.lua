
---
-- Image layer.
--
-- **Extends:** `game.map.Layer`
--
-- @classmod game.map.ImageLayer

local class = require "class"

local Layer = require "game.map.Layer"

local ImageLayer = class("game.map.ImageLayer", Layer)

---
-- Construct a new layer.
--
-- @tparam tiled.Layer data
function ImageLayer:init(data)
	assert(data.type == "imagelayer", "invalid data specified")
	Layer.init(self, data)
end

---
-- Draw the layer.
--
-- **Overrides:** `game.map.Layer:draw`
function ImageLayer:draw()
end

---
-- Update the layer logic.
--
-- **Overrides:** `game.map.Layer:update`
function ImageLayer:update()
end

return ImageLayer
