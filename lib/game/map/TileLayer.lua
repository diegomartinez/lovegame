
---
-- Tile layer.
--
-- **Extends:** `game.map.Layer`
--
-- @classmod game.map.TileLayer

local class = require "class"

local gfx = love.graphics

local Layer = require "game.map.Layer"

local TileLayer = class("game.map.TileLayer", Layer)

---
-- X origin in tiles.
-- @tfield number .
TileLayer.x = 0

---
-- Y origin in tiles.
-- @tfield number .
TileLayer.y = 0

---
-- Width in tiles.
-- @tfield number .
TileLayer.w = 0

---
-- Height in tiles.
-- @tfield number .
TileLayer.h = 0

---
-- Construct a new layer.
--
-- @tparam tiled.Layer data
-- @tparam game.map.TileSet tileset
function TileLayer:init(data, tileset)
	assert(data.type == "tilelayer", "invalid data specified")
	Layer.init(self, data)
	self.x, self.y = data.x, data.y
	self.w, self.h = data.width, data.height
	local batch = gfx.newSpriteBatch(tileset.image, data.width*data.height)
	self.batch = batch
end

---
-- Draw the layer.
--
-- **Overrides:** `game.map.Layer:draw`
function TileLayer:draw()
end

---
-- Update the layer logic.
--
-- **Overrides:** `game.map.Layer:update`
function TileLayer:update()
end

return TileLayer
