
---
-- Map.
--
-- **Extends:** `class.Object`
--
-- @classmod game.map.Map

local class = require "class"

local TileSet = require "game.map.TileSet"

local Map = class("game.map.Map")

---
-- @tfield tiled.Map .
Map.data = nil

local layerclasses = {
	tilelayer   = require "game.map.TileLayer",
	objectgroup = require "game.map.EntityLayer",
	imagelayer  = require "game.map.ImageLayer",
}

---
-- Construct a new map.
--
-- @tparam tiled.Map data  Map data. Note that this table is modified
--  in-place to hold internal state, so don't reuse it!
function Map:init(data)
	assert(data.version == "1.1", "unsupported format version")
	self.tileset = TileSet(data.tilesets[1])
	local layers = { }
	self.layers = layers
	for i, l in ipairs(data.layers) do
		local cls = (layerclasses[l.type] or error(
				"unsupported layer type: "..tostring(l.type)))
		local layer = cls(l, self.tileset)
		if l.name and l.name ~= "" then
			layers[l.name] = layer
		end
		layers[i] = layer
	end
end

return Map
