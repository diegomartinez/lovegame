
---
-- Base class for layers.
--
-- **Extends:** `class.Object`
--
-- @classmod game.map.Layer

local class = require "class"

local Layer = class("game.map.Layer")

---
-- @tfield string .
Layer.name = nil

---
-- Construct a new layer.
--
-- @tparam tiled.Layer data
function Layer:init(data)
	self.name = data.name
end

---
-- Draw the layer.
function Layer:draw()
end

---
-- Update the layer logic.
function Layer:update()
end

return Layer
