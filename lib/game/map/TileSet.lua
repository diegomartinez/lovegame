
---
-- Tile set.
--
-- **Extends:** `class.Object`
--
-- @classmod game.map.TileSet

local res = require "game.res"

local Quad = love.graphics.newQuad

local class = require "class"

local TileSet = class("game.map.TileSet")

local function basename(path)
	return path:match("([^/\\]+)%.[^.]+$")
end

---
-- Construct a new tile set.
--
-- @tparam tiled.TileSet data  Tile set data.
function TileSet:init(data)
	self.image = res.getimage(basename(data.image))
	self.imagew, self.imageh = self.image:getDimensions()
	self.tilew = data.tilewidth
	self.tileh = data.tileheight
	self.tilesperrow = math.floor(self.imagew/self.tilew)
	self.cache = { }
end

function TileSet:getquad(id)
	if id == 0 then return end
	local v = self.cache[id]
	if not v then
		id = id - 1
		local tpr = self.tilesperrow
		local tx = (id%tpr) * self.tilew
		local ty = math.floor(id/tpr) * self.tileh
		v = Quad(tx, ty, self.tilew, self.tileh, self.imagew, self.imageh)
		self.cache[id] = v
	end
	return v
end

return TileSet
