
---
-- @module game.map.entreg

local M = { }

local classtype = require "class".type

local registered = { }

---
-- Register a new class with the map system.
--
-- @tparam class.Class cls  Entity class.
function M.register(cls)
	local name = assert(classtype(cls), "invalid class specified")
	registered[name] = cls
end

---
-- Get the class for a registered entity class.
--
-- @tparam string name  Class name.
-- @return A `class.Class`. Raises an error if the name is
--  not registered.
function M.get(name)
	return assert(registered[name], "class name is not registered: "..name)
end

---
-- Create an instance of a registered class.
--
-- Equivalent to `get(name)(...)`.
--
-- @tparam string name  Class name.
-- @param ...  Arguments to instance constructor.
-- @return An instance of a subclass of `game.entity.Entity`.
function M.new(name, ...)
	return M.get(name)(...)
end

return M
