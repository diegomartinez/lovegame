
---
-- Resource management.
--
-- @module game.res

local M = { }

local function makegetter(loader)
	local cache = setmetatable({ }, { __mode="v" })
	return function(key)
		local v = cache[key]
		if not v then
			v = loader(key)
			cache[key] = v
		end
		return v
	end
end

M.getimage = makegetter(function(name)
	return love.graphics.newImage("data/gfx/"..name..".png")
end)

M.getsound = makegetter(function(name)
	return love.sound.newSource("data/sfx/"..name..".ogg")
end)

M.getmusic = makegetter(function(name)
	return love.sound.newSource("data/bgm/"..name)
end)

return M
