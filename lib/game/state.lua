
---
-- @module game.state

local M = { }

local minser = require "minser"
local open = love.filesystem.newFile

local state

---
-- @tparam number slot
-- @return A table on success, nil plus error message on error.
function M.read(slot)
	local f, e, data, ok
	f, e = open(("/save/slot%d.sav"):format(slot), "r")
	if not f then return nil, e end
	data, e = f:read()
	f:close()
	if not data then return nil, e end
	ok, data = pcall(minser.deserialize, data)
	if not ok then return nil, data end -- note: data is the error
	return data
end

---
-- @tparam number slot
-- @return A table on success, nil plus error message on error.
function M.load(slot)
	local st, e = M.read(slot)
	if not st then return nil, e end
	setmetatable(M, { __index=st, __newindex=st })
	state = st
	return st
end

---
-- @tparam number slot
-- @return True on success, nil plus error message on error.
function M.save(slot)
	assert(state, "no game started yet")
	local f, e, data, ok
	ok, data = pcall(minser.serialize, state)
	if not ok then return nil, data end -- note: data is the error
	f, e = open(("/save/slot%d.sav"):format(slot), "w")
	if not f then return nil, e end
	ok, e = f:write(data)
	if not ok then
		f:close()
		return nil, e
	end
	ok, e = f:close()
	if not ok then return nil, e end
	return true
end

---
-- @return A table.
function M.new()
	state = { }
	return state
end

---
function M.reset()
	state = nil
end

---
-- @return A table.
function M.get()
	return state
end

return M
