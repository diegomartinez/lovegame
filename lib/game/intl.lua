
---
-- @module game.intl

local M = { }

local expandvars = require "stringex".expandvars

---
-- @tparam string msg
-- @tparam ?table t
-- @return A string.
function M.gettext(msg, t)
	return (expandvars(msg, t or { }))
end

return M
