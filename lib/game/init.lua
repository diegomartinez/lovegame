
---
-- @module game

local M = { }

local open = love.filesystem.newFile

local config = require "config"

local function loadconfig()
	local conf = config()
	local f = open("game.conf")
	if f then
		local data = f:read()
		f:close()
		if data then
			conf:parse(data)
		end
	end
	return conf
end

---
-- @tfield config .
M.conf = loadconfig()

---
-- @return True on success, nil plus error message on error.
function M.saveconfig()
	local f, e, ok
	f, e = open("game.conf", "w")
	if not f then return nil, e end
	ok, e = f:write(M.conf:tostring())
	if not ok then
		f:close()
		return nil, e
	end
	ok, e = f:close()
	if not ok then return nil, e end
	return true
end

return M
