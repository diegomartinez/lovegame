
---
-- Class representing a box.
--
-- **Extends:** `class.Object`
--
-- @classmod game.Box

local class = require "class"

local Box = class("game.Box")

---
-- X coordinate.
-- @tfield number .
Box.x = 0

---
-- Y coordinate.
-- @tfield number .
Box.y = 0

---
-- Width.
-- @tfield number .
Box.w = 0

---
-- Height.
-- @tfield number .
Box.h = 0

---
-- Construct a new box.
--
-- @tparam number x  X coordinate.
-- @tparam number y  Y coordinate.
-- @tparam number w  Width.
-- @tparam number h  Height.
function Box:init(x, y, w, h)
	if type(x) == "table" then
		x, y, w, h = x.x, x.y, x.w, x.h
	end
	self.x, self.y, self.w, self.h = x, y, w, h
end

---
-- Construct a new box.
--
-- @function Box:init
-- @tparam table box  Source table. It must have fields named `x`,
--  `y`, `w`, and `h`.

---
-- Calculate the box perimeter.
--
-- @return A number.
function Box:getperimeter()
	return self.w*2 + self.h*2
end

---
-- Calculate the box area.
--
-- @return A number.
function Box:getarea()
	return self.w*self.h
end

---
-- Check if the box is empty.
--
-- @return True if the area is 0, false otherwise.
function Box:isempty()
	return self:getarea() == 0
end

---
-- Get the union between two boxes.
--
-- @tparam Box box
-- @return A box that is the union of two boxes.
function Box:union(box) -- luacheck: ignore
	error("NIY")
end

---
-- Get the difference between two boxes.
--
-- @tparam Box box
-- @return A box that is the difference of two boxes.
function Box:difference(box) -- luacheck: ignore
	error("NIY")
end

---
-- Get the intersection between two boxes.
--
-- @tparam Box box
-- @return A box that is the intersection of two boxes. If the boxes
--  have no intersection, returns an empty box.
-- @see Box:isempty
function Box:intersection(box) -- luacheck: ignore
	error("NIY")
end

---
-- Test if a point lies inside the box.
--
-- @tparam number x  X coordinate.
-- @tparam number y  Y coordinate.
-- @return True if the point is inside the box, false otherwise.
function Box:contains(x, y)
	if type(x) == "table" then
		x, y = x.x, x.y
	end
	return (x>=self.x and x<self.x+self.w
			and y>=self.y and y<self.y+self.y)
end

---
-- Test if a point lies inside the box.
--
-- @function Box:contains
-- @tparam table pt  Point. Must have `x` and `y` fields.
-- @return True if the point is inside the box, false otherwise.

---
-- Unpack the values of a box.
--
-- The return value is suitable for use with `love.graphics.rectangle`.
--
-- @return Four values: X coordinate, Y coordinate, width, and height,
--  all numbers.
function Box:unpack()
	return self.x, self.y, self.w, self.h
end

---
-- Clone this object.
--
-- @return A box covering the same area as this one.
function Box:clone()
	return Box(self.x, self.y, self.w, self.h)
end

return Box
