
---
-- Base class for entities.
--
-- **Extends:** `class.Object`
--
-- @classmod game.entity.Entity

local class = require "class"
local Box = require "game.Box"

local gfx = love.graphics

local Entity = class("game.entity.Entity")

---
-- Entity's X position.
-- @tfield number .
Entity.x = 0

---
-- Entity's Y position.
-- @tfield number .
Entity.y = 0

---
-- Entity's collision box.
-- @tfield game.Box .
Entity.bbox = Box()

---
-- @tparam table params
function Entity:init(params)
	for k, v in pairs(params) do
		self[k] = v
	end
	self.bbox = Box(self.bbox)
end

---
-- Update logic.
--
-- Override in subclasses to do something useful.
function Entity:update()
end

---
-- Draw the entity.
--
-- Do not override this method; override `ondraw` instead.
function Entity:draw()
	gfx.push()
	gfx.translate(self.x, self.y)
	self:paint()
	gfx.pop()
end

---
-- Called to draw the entity.
--
-- The origin is the entity's (X,Y) position.
--
-- Override in subclasses to do something useful.
function Entity:ondraw()
end

---
-- Called when the entity collides with the map.
--
-- Override in subclasses to do something useful.
--
-- @tparam boolean h  True if the collision happened horizontally.
-- @tparam boolean v  True if the collision happened vertically.
-- @return True to reset the coordinates to just before collision,
--  false to keep its coordinates.
function Entity:onmapcollision(h, v) -- luacheck: ignore
end

---
-- Called when the entity collides with the map.
--
-- Override in subclasses to do something useful.
--
-- @tparam game.entity.Entity other  The other entity.
-- @return True to reset the coordinates to just before collision,
--  false to keep its coordinates.
function Entity:oncollision(other) -- luacheck: ignore
end

return Entity
