
-- Ignore "unused argument 'self'"
self = false

exclude_files = {
	-- Temporary directory I use locally to hold unused files.
	"_/",
	-- Tiled tends to use long lines. Let's ignore that.
	"data/maps/"
}

-- Read-only globals.
read_globals = {
	"love",
	"game",
}

-- Only a couple of files need to modify or mutate globals.

files["main.lua"] = {
	globals = { "print", "loadfile", "game", "love" },
}

files["conf.lua"] = {
	globals = { "love" },
}

files["lib/game/main.lua"] = {
	globals = { "love" },
}
