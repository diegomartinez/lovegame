
local fs = love.filesystem
fs.setRequirePath(table.concat({
	"lib/?.lua",
	"lib/?/init.lua",
	"lib/kutils/?.lua",
	"lib/kutils/?/init.lua",
	fs.getRequirePath(),
}, ";"))

function print(...)
	local n, t = select("#", ...), { ... }
	for i = 1, n do
		t[i] = tostring(t[i])
	end
	io.stderr:write(table.concat(t, " "), "\n")
	io.stderr:flush()
end

function loadfile(filename)
	local f = assert(fs.newFile(filename))
	local code = assert(f:read())
	f:close()
	return loadstring(code)
end

local game = require "game"

local Map = require "game.map.Map"

local function loadmap(filename)
	return Map(require("minser").deserialize(
			assert(assert(fs.newFile(filename)):read())))
end

local map

function love.load()
	local conf = game.conf
	local winw = conf:getnumber("graphics", "width", 800)
	local winh = conf:getnumber("graphics", "height", 600)
	--~ local full = conf:get("graphics", "fullscreen", false)
	local borderless = conf:getboolean("graphics", "borderless", false)
	love.window.setTitle("game")
	love.window.setMode(winw, winh, {
		resizable = true,
		borderless = borderless,
		minwidth = 400,
		minheight = 300,
	})
	map = loadmap("data/maps/test.lua")
end

function love.draw()
	map:draw()
end
